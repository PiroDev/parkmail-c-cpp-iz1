CMAKE_FLAGS := -DCMAKE_BUILD_TYPE=release -S . -B build

OBJDIR := out
BINDIR := bin
PROJECT_DIR := project
SRCDIR := $(PROJECT_DIR)/src
INCDIR := $(PROJECT_DIR)/include

UNIT_TESTS_DIR := $(PROJECT_DIR)/unit_tests

CFLAGS := -std=c99 -Wall -Werror -Wextra -pedantic
CXXFLAGS := -std=c99 -Wall -Werror -Wextra -pedantic
CPPFLAGS := -I$(INCDIR)

GTEST_FLAGS := -lgtest -lgtest_main -lpthread

GCOV_FLAGS := --coverage
GCOVL := -lgcov

CC := gcc
CXX := g++

ifeq ($(mode), debug)
	CMAKE_FLAGS := -DCMAKE_BUILD_TYPE=debug -S . -B build
	CFLAGS += -g3 -ggdb
	CXXFLAGS += -g3 -ggdb
endif

install : CMakeLists.txt
	cmake $(CMAKE_FLAGS)
	cd build;	make install

lint : linters/run.sh
	./linters/run.sh

unit: $(BINDIR)/unit_tests.exe
	echo "RUN unit testing with valgrind"
	valgrind --leak-check=full ./bin/unit_tests.exe
	gcovr out -o $(OBJDIR)/coverage_report.txt
	cat $(OBJDIR)/coverage_report.txt

$(BINDIR)/unit_tests.exe: $(OBJDIR)/users_functions.o $(OBJDIR)/test_users_functions.o $(OBJDIR)/test_main.o
	mkdir -p $(BINDIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $(BINDIR)/unit_tests.exe $^ $(GTEST_FLAGS) $(GCOVL)

$(OBJDIR)/test_%.o : $(UNIT_TESTS_DIR)/test_%.cpp $(INCDIR)/*.h
	mkdir -p $(OBJDIR)
	$(CXX) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(INCDIR)/*.h
	mkdir -p $(OBJDIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(GCOV_FLAGS) -c $< -o $@

.PHONY : clean
clean :
	rm -rf bin build out
