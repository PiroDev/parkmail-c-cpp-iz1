#!/usr/bin/env bash

set -e
set -o pipefail

function print_header() {
    echo -e "\n***** ${1} *****"
}

print_header "RUN cppcheck"
cppcheck project --enable=all --error-exitcode=1 -I project/include --suppress=missingIncludeSystem

print_header "RUN cpplint.py for sources"
python2.7 ./linters/cpplint/cpplint.py --extensions=c project/include/*.h project/src/*.c

print_header "RUN cpplint.py for unit_tests"
python2.7 ./linters/cpplint/cpplint.py --extensions=cpp project/unit_tests/*.cpp

print_header "SUCCESS"
