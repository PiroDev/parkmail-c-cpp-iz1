#ifndef PROJECT_INCLUDE_STATUS_CODES_H_
#define PROJECT_INCLUDE_STATUS_CODES_H_

typedef enum {
    ok = 0,
    error_cannot_read_str = 1,
    error_out_of_memory = 2,
    error_wrong_role = 3,
    error_wrong_member_type = 4,
    error_wrong_authority = 5,
    error_empty_input = 6,
    eof_reached = 7,
    error_nullptr_args = 8
} status_code;

#endif  // PROJECT_INCLUDE_STATUS_CODES_H_
