#ifndef PROJECT_INCLUDE_USERS_IO_H_
#define PROJECT_INCLUDE_USERS_IO_H_

#include <stdio.h>

#include "status_codes.h"
#include "users.h"

status_code read_users_to_array(FILE *f, users_array_t *users);
status_code read_user(FILE *f, user_t *user);
status_code get_search_keys(FILE *f, user_t *user);
void print_users_array(FILE *f, const users_array_t *users);
void print_user(FILE *f, const user_t *user);
void print_error(status_code error);

#endif  // PROJECT_INCLUDE_USERS_IO_H_
