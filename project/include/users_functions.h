#ifndef PROJECT_INCLUDE_USERS_FUNCTIONS_H_
#define PROJECT_INCLUDE_USERS_FUNCTIONS_H_

#include "status_codes.h"
#include "users.h"

#define INIT_ARRAY_SIZE 10
#define ARRAY_SIZE_MULTIPLIER 2

users_array_t *create_users_array(void);
status_code push_user_to_array(users_array_t *users, const user_t *user);
void free_users_array(users_array_t **users);
void free_user(user_t *user);
void select_users(const users_array_t *users, users_array_t *selected, const user_t *user);
int user_match(const user_t *user_1, const user_t *user_2);
status_code copy_user(const user_t *from, user_t *to);

#endif  // PROJECT_INCLUDE_USERS_FUNCTIONS_H_
