#ifndef PROJECT_INCLUDE_USERS_H_
#define PROJECT_INCLUDE_USERS_H_

typedef enum { owner = 1, client = 2, developer = 3 } role_t;

typedef enum { common = 1, sponsor = 2, main_sponsor = 3 } authority_t;

typedef enum { company = 1, individual = 2 } member_t;

typedef struct {
    char *name;
    char *project_name;
    role_t role;
    member_t member_type;
    authority_t authority;
} user_t;

typedef struct {
    user_t *data;
    int count_elems;
    int size;
} users_array_t;

#endif  // PROJECT_INCLUDE_USERS_H_
