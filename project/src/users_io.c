#define _GNU_SOURCE

#include "users_io.h"

#include <stdlib.h>
#include <string.h>

#include "users_functions.h"

void clrscr(void) {
    if (system("clear")) system("cls");
}

status_code read_users_to_array(FILE *f, users_array_t *users) {
    status_code result = ok;

    while (!result) {
        user_t user = { 0 };
        result = read_user(f, &user);

        if (!result) result = push_user_to_array(users, &user);

        free_user(&user);
    }

    if (users->count_elems == 0)
        result = error_empty_input;
    else if (result == eof_reached)
        result = ok;

    return result;
}

status_code read_user(FILE *f, user_t *user) {
    status_code result = ok;
    printf("\nИмя участника (название компании): ");
    size_t str_len = 0;
    ssize_t read = 0;

    if ((read = getline(&user->name, &str_len, f)) != -1) {
        if (user->name[read - 1] == '\n') user->name[read - 1] = '\0';

        if (!strcmp(user->name, "0")) result = eof_reached;
    } else {
        free(user->name);
        result = error_cannot_read_str;
    }

    if (!result) {
        str_len = 0;
        printf("Название проекта: ");
        if ((read = getline(&user->project_name, &str_len, f)) != -1) {
            if (user->project_name[read - 1] == '\n') user->project_name[read - 1] = '\0';

            if (!strcmp(user->project_name, "0")) result = eof_reached;
        } else {
            free(user->project_name);
            result = error_cannot_read_str;
        }
    }

    if (!result) {
        int member_type = 0;
        printf("Вид лица (1 - юридическое (компания), 2 - физическое): ");
        member_type = fgetc(f) - '0';
        fgetc(f);
        switch (member_type) {
            case company:
                user->member_type = company;
                break;

            case individual:
                user->member_type = individual;
                break;

            case '0':
                result = eof_reached;
                break;

            default:
                result = error_wrong_member_type;
                break;
        }
    }

    if (!result) {
        int role = 0;
        printf("Роль в проекте (1 - заказчик, 2 - клиент, 3 - разработчик): ");
        role = fgetc(f) - '0';
        fgetc(f);
        switch (role) {
            case owner:
                user->role = owner;
                break;

            case client:
                user->role = client;
                break;

            case developer:
                user->role = developer;
                break;

            case '0':
                result = eof_reached;
                break;

            default:
                result = error_wrong_role;
                break;
        }
    }

    if (!result) {
        int authority = 0;
        printf("Степень влияния (1 - участник, 2 - спонсор, 3 - главный спонсор): ");
        authority = fgetc(f) - '0';
        fgetc(f);
        switch (authority) {
            case common:
                user->authority = common;
                break;

            case sponsor:
                user->authority = sponsor;
                break;

            case main_sponsor:
                user->authority = main_sponsor;
                break;

            case '0':
                result = eof_reached;
                break;

            default:
                result = error_wrong_authority;
                break;
        }
    }

    return result;
}

status_code get_search_keys(FILE *f, user_t *keys) {
    status_code result = ok;

    clrscr();

    printf("Для какого проекта ищем заинтересованных лиц?\n");
    printf("Название проекта: ");

    ssize_t read = 0;
    size_t str_len = 0;

    if ((read = getline(&keys->project_name, &str_len, f)) != -1) {
        if (keys->project_name[read - 1] == '\n') keys->project_name[read - 1] = '\0';
    } else {
        free(keys->project_name);
        result = error_cannot_read_str;
    }

    if (!result) {
        int role = 0;
        printf("Кого ищем? (1 - заказчиков, 2 - клиентов, 3 - разработчиков): ");
        role = fgetc(f) - '0';
        fgetc(f);
        switch (role) {
            case owner:
                keys->role = owner;
                break;

            case client:
                keys->role = client;
                break;

            case developer:
                keys->role = developer;
                break;

            default:
                result = error_wrong_role;
                break;
        }
    }

    return result;
}

void print_users_array(FILE *f, const users_array_t *users) {
    fprintf(f, "\n");
    if (users->count_elems == 0) {
        fprintf(f, "Участников нет!\n");
    } else {
        for (int i = 0; i < users->count_elems; i++)
            print_user(f, users->data + i);
    }
}

void print_user(FILE *f, const user_t *user) {
    switch (user->member_type) {
        case company:
            fprintf(f, "Компания \"%s\" - ", user->name);
            break;

        case individual:
            fprintf(f, "%s - ", user->name);
            break;

        default:
            break;
    }

    switch (user->role) {
        case owner:
            fprintf(f, "заказчик ");
            break;

        case client:
            fprintf(f, "клиент ");
            break;

        case developer:
            fprintf(f, "разработчик ");
            break;

        default:
            break;
    }

    switch (user->authority) {
        case sponsor:
            fprintf(f, "и спонсор ");
            break;

        case main_sponsor:
            fprintf(f, "и главный споносор ");

        default:
            break;
    }

    fprintf(f, "проекта %s\n", user->project_name);
}

void print_error(status_code error) {
    switch (error) {
        case error_cannot_read_str:
            printf("\nОшибка: не удалось прочитать строку!\n");
            break;

        case error_out_of_memory:
            printf("\nОшибка: не удалось выделить память!\n");
            break;

        case error_wrong_role:
            printf("\nОшибка: несуществующая роль!\n");
            break;

        case error_wrong_member_type:
            printf("\nОшибка: несуществующий тип участника!\n");
            break;

        case error_wrong_authority:
            printf("\nОшибка: несуществующий уровень влияния участника!\n");
            break;

        case error_empty_input:
            printf("\nОшибка: информация ни об одном участнике не была считана!\n");
            break;

        default:
            break;
    }
}
