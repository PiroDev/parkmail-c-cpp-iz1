#include "status_codes.h"
#include "users_functions.h"
#include "users_io.h"

int main(void) {
    status_code result = ok;
    users_array_t *users = create_users_array();

    if (!users) {
        print_error(error_out_of_memory);
        return error_out_of_memory;
    }

    printf("Вводите информацию об участниках проектов (0 - признак конца ввода)\n");
    result = read_users_to_array(stdin, users);

    if (!result) {
        user_t keys = { 0 };
        result = get_search_keys(stdin, &keys);

        if (!result) {
            users_array_t *search_result = create_users_array();
            select_users(users, search_result, &keys);

            print_users_array(stdout, search_result);

            free_users_array(&search_result);
        }

        free_user(&keys);
    }

    free_users_array(&users);

    if (result)
        print_error(result);

    return result;
}
