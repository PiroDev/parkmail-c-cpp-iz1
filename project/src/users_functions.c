#include "users_functions.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

users_array_t *create_users_array(void) {
    users_array_t *users = (users_array_t *)malloc(sizeof(users_array_t));

    if (users) {
        users->size = INIT_ARRAY_SIZE;
        users->count_elems = 0;

        users->data = (user_t *)calloc(INIT_ARRAY_SIZE, sizeof(user_t));

        if (!users->data) {
            free(users);
            users = NULL;
        }
    }

    return users;
}

status_code push_user_to_array(users_array_t *users, const user_t *user) {
    status_code result = ok;

    if (!users || !user)
        return error_nullptr_args;

    if (users->count_elems >= users->size) {
        user_t *tmp = realloc(users->data, users->size * ARRAY_SIZE_MULTIPLIER * sizeof(user_t));
        if (!tmp) {
            result = error_out_of_memory;
        } else {
            users->data = tmp;
            users->size *= ARRAY_SIZE_MULTIPLIER;
        }
    }

    if (!result) {
        user_t null_user = { 0 };
        *(users->data + users->count_elems) = null_user;
        copy_user(user, users->data + users->count_elems);
        users->count_elems += 1;
    }

    return result;
}

void select_users(const users_array_t *users, users_array_t *selected, const user_t *keys) {
    for (int i = 0; i < users->count_elems; i++) {
        if (!user_match(users->data + i, keys)) continue;
        user_t tmp = { 0 };

        copy_user(users->data + i, &tmp);
        push_user_to_array(selected, &tmp);

        free_user(&tmp);
    }
}

int user_match(const user_t *user, const user_t *keys) {
    return (!strcmp(user->project_name, keys->project_name) && (user->role == keys->role));
}

void free_users_array(users_array_t **users) {
    if (*users) {
        for (int i = 0; i < (*users)->count_elems; i++)
            free_user((*users)->data + i);
    }

    free((*users)->data);
    (*users)->data = NULL;
    free(*users);

    *users = NULL;
}

void free_user(user_t *user) {
    free(user->name);
    user->name = NULL;
    free(user->project_name);
    user->project_name = NULL;
}

status_code copy_user(const user_t *from, user_t *to) {
    status_code result = ok;

    if (to->name) {
        free(to->name);
        to->name = NULL;
    }

    to->name = (char *) malloc((strlen(from->name) + 1) * sizeof(char));
    if (to->name) {
        snprintf(to->name, strlen(from->name) + 1, "%s", from->name);

        if (to->project_name) {
            free(to->project_name);
            to->project_name = NULL;
        }

        to->project_name = (char *) malloc((strlen(from->project_name) + 1) * sizeof(char));
        if (to->project_name) {
            snprintf(to->project_name, strlen(from->project_name) + 1, "%s", from->project_name);

            to->member_type = from->member_type;
            to->role = from->role;
            to->authority = from->authority;
        } else {
            result = error_out_of_memory;
        }
    } else {
        result = error_out_of_memory;
    }

    return result;
}
