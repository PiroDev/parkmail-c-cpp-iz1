#include <gtest/gtest.h>
#include <string.h>

extern "C" {
#include "users_functions.h"
}

bool is_users_equal(const user_t *user_1, const user_t *user_2) {
    return user_1->authority == user_2->authority && user_1->member_type == user_2->member_type &&
           !strcmp(user_1->name, user_2->name) && !strcmp(user_1->project_name, user_2->project_name);
}

bool is_users_arrays_equal(const users_array_t *users_1, const users_array_t *users_2) {
    bool result = true;

    result = users_1->count_elems == users_2->count_elems;

    for (int i = 0; result && i < users_1->count_elems; i++)
        result = is_users_equal(users_1->data + i, users_2->data + i);

    return result;
}

void initialize_user(user_t *user, const char *name, const char *project_name, role_t role,
                     member_t member_type, authority_t authority) {
    user->name = strdup(name);
    user->project_name = strdup(project_name);
    user->role = role;
    user->member_type = member_type;
    user->authority = authority;
}

TEST(create_users_array_positive, successfully_create_users_array) {
    users_array_t *users = create_users_array();

    ASSERT_TRUE(users != NULL);

    free_users_array(&users);
}

TEST(push_user_to_array_positive, push_one_user_to_array_without_extention) {
    users_array_t *users = create_users_array();
    size_t init_array_size = users->size;
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name", owner, company, common);

    ASSERT_EQ(push_user_to_array(users, &user), ok);
    ASSERT_EQ(init_array_size, users->size);
    ASSERT_TRUE(is_users_equal(&user, users->data));

    free_user(&user);
    free_users_array(&users);
}

TEST(push_user_to_array_positive, push_multiply_users_to_array_without_extention) {
    users_array_t *users = create_users_array();
    size_t init_array_size = users->size;
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name", owner, company, common);

    for (int i = 0; i < init_array_size; i++) {
        ASSERT_EQ(push_user_to_array(users, &user), ok);
        ASSERT_TRUE(is_users_equal(&user, users->data + i));
    }
    ASSERT_EQ(init_array_size, users->size);

    free_user(&user);
    free_users_array(&users);
}

TEST(push_user_to_array_positive, push_users_to_array_with_extention) {
    users_array_t *users = create_users_array();
    size_t init_array_size = users->size;
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name", owner, company, common);

    for (int i = 0; i <= init_array_size; i++) {
        ASSERT_EQ(push_user_to_array(users, &user), ok);
        ASSERT_TRUE(is_users_equal(&user, users->data + i));
    }
    ASSERT_LT(init_array_size, users->size);

    free_user(&user);
    free_users_array(&users);
}

TEST(select_users_positive, select_owners) {
    users_array_t *users = create_users_array();
    users_array_t *expected_users = create_users_array();
    users_array_t *selectes_users = create_users_array();

    const size_t name_len = strlen("Test name 9") + 1;
    const char project_name[] = "Test project name";

    user_t key = { 0 };
    initialize_user(&key, "Test name", project_name, owner, company, common);

    for (int i = 0; i < users->size; i++) {
        user_t user = { 0 };
        char name[name_len] = { 0 };
        snprintf(name, name_len, "Test name %d", i);

        role_t role = (role_t) (i % 3 + 1);
        initialize_user(&user, name, project_name, role, company, common);

        push_user_to_array(users, &user);
        if (role == owner)
            push_user_to_array(expected_users, &user);

        free_user(&user);
    }

    select_users(users, selectes_users, &key);
    free_user(&key);

    ASSERT_TRUE(is_users_arrays_equal(expected_users, selectes_users));

    free_users_array(&users);
    free_users_array(&expected_users);
    free_users_array(&selectes_users);
}

TEST(select_users_positive, select_clients) {
    users_array_t *users = create_users_array();
    users_array_t *expected_users = create_users_array();
    users_array_t *selectes_users = create_users_array();

    const size_t name_len = strlen("Test name 9") + 1;
    const char project_name[] = "Test project name";

    user_t key = { 0 };
    initialize_user(&key, "Test name", project_name, client, company, common);

    for (int i = 0; i < users->size; i++) {
        user_t user = { 0 };
        char name[name_len] = { 0 };
        snprintf(name, name_len, "Test name %d", i);

        role_t role = (role_t) (i % 3 + 1);
        initialize_user(&user, name, project_name, role, company, common);

        push_user_to_array(users, &user);
        if (role == client)
            push_user_to_array(expected_users, &user);

        free_user(&user);
    }

    select_users(users, selectes_users, &key);
    free_user(&key);

    ASSERT_TRUE(is_users_arrays_equal(expected_users, selectes_users));

    free_users_array(&users);
    free_users_array(&expected_users);
    free_users_array(&selectes_users);
}

TEST(select_users_positive, select_developers) {
    users_array_t *users = create_users_array();
    users_array_t *expected_users = create_users_array();
    users_array_t *selectes_users = create_users_array();

    const size_t name_len = strlen("Test name 9") + 1;
    const char project_name[] = "Test project name";

    user_t key = { 0 };
    initialize_user(&key, "Test name", project_name, developer, company, common);

    for (int i = 0; i < users->size; i++) {
        user_t user = { 0 };
        char name[name_len] = { 0 };
        snprintf(name, name_len, "Test name %d", i);

        role_t role = (role_t) (i % 3 + 1);
        initialize_user(&user, name, project_name, role, company, common);

        push_user_to_array(users, &user);
        if (role == developer)
            push_user_to_array(expected_users, &user);

        free_user(&user);
    }

    select_users(users, selectes_users, &key);
    free_user(&key);

    ASSERT_TRUE(is_users_arrays_equal(expected_users, selectes_users));

    free_users_array(&users);
    free_users_array(&expected_users);
    free_users_array(&selectes_users);
}

TEST(user_match_positive, user_match) {
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name", developer, company, common);

    user_t key = { 0 };
    initialize_user(&key, "Test name", "Test project name", developer, company, common);

    GTEST_ASSERT_NE(user_match(&user, &key), 0);

    free_user(&user);
    free_user(&key);
}

TEST(user_match_positive, user_no_match_by_project_name) {
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name 1", developer, company, common);

    user_t key = { 0 };
    initialize_user(&key, "Test name", "Test project name", developer, company, common);

    ASSERT_EQ(user_match(&user, &key), 0);

    free_user(&user);
    free_user(&key);
}

TEST(user_match_positive, user_no_match_by_role) {
    user_t user = { 0 };
    initialize_user(&user, "Test name", "Test project name", owner, company, common);

    user_t key = { 0 };
    initialize_user(&key, "Test name", "Test project name", developer, company, common);

    GTEST_ASSERT_EQ(user_match(&user, &key), 0);

    free_user(&user);
    free_user(&key);
}

TEST(copy_user_positive, copy_user_to_null_initialized_one) {
    user_t from = { 0 };
    initialize_user(&from, "Test name", "Test project name", owner, company, common);

    user_t to = { 0 };

    copy_user(&from, &to);

    ASSERT_TRUE(is_users_equal(&from, &to));

    free_user(&from);
    free_user(&to);
}

TEST(copy_user_positive, copy_user_to_not_null_initialized_one) {
    user_t from = { 0 };
    initialize_user(&from, "Test name", "Test project name", owner, company, common);

    user_t to = { 0 };
    initialize_user(&to, "Test name 1", "Test project name 2", developer, individual, sponsor);

    copy_user(&from, &to);

    ASSERT_TRUE(is_users_equal(&from, &to));

    free_user(&from);
    free_user(&to);
}
